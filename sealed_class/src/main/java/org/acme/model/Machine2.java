package org.acme.model;

abstract sealed class Machine2 {

    int nbOfWheels;

    public Machine2(int nbOfWheels) {
        this.nbOfWheels = nbOfWheels;
    }

    public static final class Car2 extends Machine2 {
        public Car2() {
            super(4);
        }
    }

    public final class Truck2 extends Machine2 {
        public Truck2() {
            super(6);
        }
    }

    public final class Motorcycle2 extends Machine2 {
        public Motorcycle2() {
            super(2);
        }
    }

    public int getNbOfWheels() {
        return nbOfWheels;
    }

    public void setNbOfWheels(int nbOfWheels) {
        this.nbOfWheels = nbOfWheels;
    }
}

