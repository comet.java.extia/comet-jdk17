package org.acme.model;

abstract sealed class Machine permits Car, Truck, Motorcycle {

    int nbOfWheels;

    public Machine(int nbOfWheels) {
        this.nbOfWheels = nbOfWheels;
    }

    public int getNbOfWheels() {
        return nbOfWheels;
    }

}
