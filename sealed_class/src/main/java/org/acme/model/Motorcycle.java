package org.acme.model;

public sealed class Motorcycle extends Machine permits Suzuki, Honda {

    public Motorcycle() {
        super(2);
    }
}
