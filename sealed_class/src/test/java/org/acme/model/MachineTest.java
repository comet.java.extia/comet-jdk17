package org.acme.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MachineTest {

    @Test
    public void test() {
        var car = new Car();

        Assertions.assertEquals(4, car.getNbOfWheels());
    }

    @Test
    public void test3() {
        var car2 = new Machine2.Car2();

        Assertions.assertEquals(4, car2.getNbOfWheels());
    }

    @Test
    public void test4() {
        var tesla = new Honda();

        Assertions.assertEquals(4, tesla.getNbOfWheels());
    }

    @Test
    public void test5() {
        var dodge = new Dodge();

        Assertions.assertEquals(4, dodge.getNbOfWheels());
    }

}