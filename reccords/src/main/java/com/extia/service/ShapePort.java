package com.extia.service;

import java.util.Collection;

import com.extia.domain.Shape;

public interface ShapePort {
    
    Collection<Shape> getAllShape();
}
