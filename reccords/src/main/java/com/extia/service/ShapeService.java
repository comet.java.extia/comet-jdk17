package com.extia.service;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import com.extia.infra.ShapeDtoMapper;
import com.extia.infra.ShapesDto;

@RequestScoped
public class ShapeService {

    @Inject
    ShapePort shapePort;
    
    @Inject
    ShapeDtoMapper dtoMapper;
    
    public ShapesDto getShapes()
    {
        return new ShapesDto( shapePort.getAllShape().stream().map(dtoMapper::toDto).toList() );
    }

}
