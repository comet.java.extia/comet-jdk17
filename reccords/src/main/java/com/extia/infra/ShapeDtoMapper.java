package com.extia.infra;

import javax.enterprise.context.RequestScoped;

import com.extia.domain.Shape;
import com.extia.domain.Square;

@RequestScoped
public class ShapeDtoMapper {
    
    public ShapeDto toDto(Shape shape)
    {
        return new ShapeDto(shape.getPoints());
    }

    public Shape toBusinessSquare(ShapeDto dto)
    {
        return new Square(dto.getPoints());
    }

    
}
