package com.extia.infra.ws;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.extia.infra.ShapesDto;
import com.extia.service.ShapeService;

@Path("shapes")
public class ShapesResources {

    @Inject
    ShapeService service;

    @GET
    @Path("all")
    @Produces(MediaType.APPLICATION_JSON)
    public ShapesDto getAll() {
        return service.getShapes();
    }
}