package com.extia.infra;

import java.util.Collection;
import java.util.Collections;
import java.util.Objects;

import javax.json.bind.annotation.JsonbCreator;
import javax.json.bind.annotation.JsonbProperty;

public class ShapesDto {
    
    @JsonbProperty(value = "shapes",nillable = false)
    private final Collection<ShapeDto> shapes;

    private final int nbShapes;

    @JsonbCreator
    public ShapesDto(Collection<ShapeDto> shapes) {
        this.shapes = Objects.requireNonNull(shapes);
        this.nbShapes = shapes.size();
    }

    public int getNbShapes() {
        return nbShapes;
    }

    public Collection<ShapeDto> getShapes() {
        return Collections.unmodifiableCollection(shapes);
    }
}
