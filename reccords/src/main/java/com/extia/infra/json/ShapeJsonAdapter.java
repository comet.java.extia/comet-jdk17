package com.extia.infra.json;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.json.bind.JsonbBuilder;

import com.extia.domain.Shape;
import com.extia.domain.Square;
import com.extia.infra.ShapeDto;
import com.extia.infra.ShapeDtoMapper;
import com.extia.service.ShapePort;

@ApplicationScoped
public class ShapeJsonAdapter implements ShapePort {

    @Inject
    ShapeDtoMapper dtoMapper;

    private static final String SQUARE_JSON_SOURCE = """
            [
                {
                    "points": [{"x": 5,"y": 10},{"x":5,"y": 5},{"x": 10,"y": 5},{"x": 10,"y": 10}]
                }   , 

                {
                    "points": [{"x": -5,"y": 10},{"x":5,"y": -5},{"x":10,"y": 5},{"x":-10,"y": 10}]
                } 

            ]
            """;

    @Override
    public Collection<Shape> getAllShape() {

        try (var jsonParser = JsonbBuilder.create()) {

            return Arrays.asList(jsonParser.fromJson(SQUARE_JSON_SOURCE,ShapeDto[].class)).stream().map(dtoMapper::toBusinessSquare).toList();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return Collections.emptyList();
    }

}
