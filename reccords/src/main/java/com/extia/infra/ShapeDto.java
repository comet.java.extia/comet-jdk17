package com.extia.infra;

import java.util.Collection;
import java.util.Collections;

import javax.json.bind.annotation.JsonbCreator;

import com.extia.domain.Point;

public class ShapeDto {

    private final Collection<Point> points;

    @JsonbCreator
    public ShapeDto(Collection<Point> points) {
        this.points = points;
    }

    public Collection<Point> getPoints() {
        return Collections.unmodifiableCollection(points);
    }

}