package com.extia.domain;

import java.util.Collection;

public class Square extends Shape {

    @Override
    int getNbPoint() {
        return 4;
    }

    public Square(Collection<Point> points)
    {
        super(points);
    }
    
}
