package com.extia.domain;

public record Point(int x, int y) {
    
}
