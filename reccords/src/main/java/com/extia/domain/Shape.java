package com.extia.domain;

import java.util.Collection;
import java.util.Objects;

public abstract class Shape{

    protected final Collection<Point> points;

    abstract int getNbPoint();

    public Collection<Point> getPoints(){
        return points;
    }

    protected Shape(Collection<Point> points)
    {
        Objects.requireNonNull(points);
        this.points = points;
        if(points.size() != getNbPoint())
        {
            throw new IllegalArgumentException();
        }

    }

}