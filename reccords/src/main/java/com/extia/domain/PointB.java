package com.extia.domain;

import java.util.Objects;

public record PointB(int x, int y) {
    
    public PointB(int x, int y )
    {
        this.x = x;
        this.y = Objects.requireNonNull(y);
    }

}
