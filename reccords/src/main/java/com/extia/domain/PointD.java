package com.extia.domain;

import java.util.Arrays;
import java.util.Objects;

public record PointD(int x, int y) {
    
    public PointD
    {
        Objects.requireNonNull(y);
        Objects.requireNonNull(x);
        x = y - x;
    }

    public int x()
    {
        return x;
    }

    public int xMultiplyByY()
    {
        return x*y;
    }

    public void newMethodReflection()
    {
        this.getClass().isRecord();
        this.getClass().getRecordComponents();      
    }

}
