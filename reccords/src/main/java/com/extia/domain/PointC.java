package com.extia.domain;

import java.util.Objects;

public record PointC(int x, int y) {
    
    public PointC(int x, int y )
    {
        this.x = x;
        this.y = Objects.requireNonNull(y);
    }

    public int x()
    {
        return x;
    }

    public int xMultiplyByY()
    {
        return x*y;
    }

}
