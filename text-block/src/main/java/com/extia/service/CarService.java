package com.extia.service;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.enterprise.context.RequestScoped;

import com.extia.infra.DataType;
import com.extia.infra.jpa.Car;

@RequestScoped
public class CarService {


    public String getCarsByType(DataType type)
    {

       return switch(type)
        {
            case JSON -> '[' + mock().map(CarWithMotor::getAsJSON).collect(Collectors.joining(",")) +']';
            case SQL -> mock().map(CarWithMotor::getAsSQL).collect(Collectors.joining(";\r\n"));
            case TEXT -> mock().map(CarWithMotor::toString).collect(Collectors.joining("\r\n"));
        };
    }

    private Stream<CarWithMotor> mock()
    {
        return Car.<Car>streamAll()
        .map(car -> new CarWithMotor("%s %s".formatted(car.brand.name(),car.model), car.color, new Motor(Math.abs(car.model.charAt(0)),0.95)));
    }
    record Motor(int power,double multiplicator)
    {
        double getVMax()
        {
            return power*multiplicator;
        }
    }

    record CarWithMotor(String name, String color, Motor motor){

        String getAsJSON(){
            return """
                {
                    "name": "%s",
                    "color": "%s",
                    "power" : %s,
                    "vMax" : %s
                }
            """.formatted(this.name,this.color,this.motor.power,this.motor.getVMax());
        }

        String getAsSQL()
        {            
            return """
                        INSERT INTO %s (name,color,vmax) \
                        VALUES ('%s','%s',vmax)"""
            .formatted(CarWithMotor.class.getSimpleName(),name,color,motor.getVMax());
        }
       
        @Override
        public String toString()
        {
            return """
                My %s
                is %s, it's vMax is %s""".formatted(name,color,motor.getVMax());
        }      
    }
}