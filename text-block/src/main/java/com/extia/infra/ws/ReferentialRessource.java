package com.extia.infra.ws;

import java.util.stream.Collectors;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

import com.extia.infra.DataType;
import com.extia.service.CarService;

import org.jboss.resteasy.spi.HttpRequest;

import lombok.extern.slf4j.Slf4j;

@Path("ref")
@RequestScoped
@Slf4j
public class ReferentialRessource {

  @Inject
  CarService carService;

  @Context
  HttpRequest context;

  @GET
  @Path("{type}")
  @Produces(value = { MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.WILDCARD })
  public Response getCars(@PathParam("type") DataType type) {
   String responseStr = carService.getCarsByType(type);
   traceRequest(responseStr);
    return  Response.ok(responseStr)
        .cookie(new NewCookie("MyCookie", "with milk")).build();
      
  }

  private void traceRequest(String bodyResponse) {
    log.info("""

        TRACE_REQUEST -> {} {}
        PARAMS -> {}
        FROM -> {}
        Cookies -> {}

        BODY Response : 
        {}
        """,
        context.getHttpMethod(), context.getUri().getBaseUri(),
        context.getUri().getPathParameters(),
        context.getRemoteHost(),
        context.getHttpHeaders().getCookies().entrySet().stream()
            .map(entry -> entry.getValue().getName() + " : " + entry.getValue().getValue())
            .collect(Collectors.joining("\n")),bodyResponse);
  }

}
