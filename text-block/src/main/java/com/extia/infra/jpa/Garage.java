package com.extia.infra.jpa;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

@Entity
@Table(name = "garage")
public class Garage extends PanacheEntity {
    
    @Column(name = "name")
    public String name;
    
    @ManyToMany(targetEntity = Car.class)
    @JoinTable(name = "stock", joinColumns = {
        @JoinColumn(name = "id_garage")
    }
    ,inverseJoinColumns = {
        @JoinColumn(name = "id_car")
    })
    public List<Car> cars;

    @Override
    public String toString()
    {
        return "Garage " + name;
    }
}
