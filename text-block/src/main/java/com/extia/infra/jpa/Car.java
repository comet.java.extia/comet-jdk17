package com.extia.infra.jpa;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.JoinColumn;

import com.extia.domain.BrandCar;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

@Entity
@Table(name = "car")
public class Car extends PanacheEntity{
    
    @Column(name = "brand")
    @Enumerated(EnumType.STRING)
    public BrandCar brand;

    @Column(name = "model")
    public String model;

    @Column(name = "color")
    public String color;

    @ManyToMany(targetEntity = Garage.class)
    @JoinTable(name = "stock",joinColumns = {
        @JoinColumn(name = "id_car")
    }
    ,inverseJoinColumns = {
        @JoinColumn(name = "id_garage")
    })
    public List<Garage> garage = new ArrayList<>();


    @Override
    public String toString()
    {
        return "Car " + brand.name() + " model " + model + " color " + color + " IN : " + (garage.size() > 0 ? garage.get(0) : "No garage");
    }
}
