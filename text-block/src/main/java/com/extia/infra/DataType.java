package com.extia.infra;

public enum DataType {
    SQL,
    TEXT,
    JSON
}
