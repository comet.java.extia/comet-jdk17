
package com.extia.domain;

public enum BrandCar{

    DODGE,
    TESLA,
    PEUGEOT;
}