package com.extia.textblock;

public interface OthersCases {
    
    String withAccolade = """
        "Add quote text"
        Escape \"""

    """;

    String forWindowswithCarrier = """    
        separated with\r
        TextHere\r        
    """;
    
    //Also newline terminated with  \n only
    String ingoreANewLine = """    
        This line have not \
        new line
        """;
        
    String addExplicitSpace = """    
        This line have not\s 
        new line
        """;

    String withFormatted = """    
        Hi I'm %s, the new teammate
        """.formatted("John");
}
