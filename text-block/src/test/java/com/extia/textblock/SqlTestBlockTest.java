package com.extia.textblock;

import java.util.Arrays;
import java.util.stream.Stream;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.extia.domain.BrandCar;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import io.quarkus.test.TestTransaction;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.h2.H2DatabaseTestResource;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
@QuarkusTestResource(H2DatabaseTestResource.class)
@TestTransaction
class SqlTestBlockTest implements TestBlockTextHelper{
    
    @Inject
    EntityManager entityManager;

    @BeforeAll
    public static void setupAll()
    {
        System.out.println(Banner.SQL.getBanner());
    }

    static String old_query = 
    "SELECT c.brand,count(c.brand) \n"+
    "FROM Car c \n"+
    "WHERE c.brand IN (:brand) \n"+
    "GROUP BY c.brand\n";


    static String new_query_with_jdk_17 = """
    SELECT c.brand,count(c.brand)
    FROM Car c
    WHERE c.brand IN (:brand)
    GROUP BY c.brand
    """;

    public static Stream<Arguments> inputQuery()
    {
        return Stream.of(Arguments.of(old_query),Arguments.of(new_query_with_jdk_17));
    }


    @ParameterizedTest
    @MethodSource("inputQuery")
    void before_jdk17_sqlCode(String sqlQuery)
    {
        TypedQuery<Object[]> carsQuery = entityManager.createQuery(sqlQuery,Object[].class);

        carsQuery.setParameter("brand", Arrays.stream(BrandCar.values()).toList());
 
        carsQuery.getResultList().stream()
        .map(Arrays::toString)
        .forEach(System.out::println);

    }

    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
}