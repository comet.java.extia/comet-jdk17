package com.extia.textblock;

import java.util.ArrayList;
import java.util.List;
import java.util.random.RandomGenerator;
import java.util.random.RandomGeneratorFactory;
import java.util.stream.Stream;

import javax.persistence.EntityManager;

import com.extia.domain.BrandCar;
import com.extia.infra.jpa.Car;
import com.extia.infra.jpa.Garage;

public interface TestBlockTextHelper {
    
    EntityManager getEntityManager();

    RandomGeneratorFactory<RandomGenerator> random = RandomGeneratorFactory.getDefault(); ;

    default void loadMock()
    {
        List<Garage> idsGarage = new ArrayList<>();

        Stream.of(new GarageMock("NANTES AUTO"), new GarageMock("TOURS AUTO"))
        .forEach((garage) -> {
            Garage gar = new Garage();
            gar.name = garage.name;
            gar.persist();
            idsGarage.add(gar);
        } );

        Stream.of(new CarMock("YELLOW"),new CarMock("RED"),new CarMock("RED"),new CarMock("RED"))
        .forEach((mock) ->{
            Car car = new Car();
            car.brand = mock.brand;
            car.color = mock.color;
            car.model = mock.model;
            int randomId = random.create().nextInt(idsGarage.size()-1)+1;
            car.garage = List.of(idsGarage.get(randomId));
            car.persistAndFlush();
        } );
    }
   
    /**
     * GarageMock
     */
    public record GarageMock(String name)  {
    }
    public record CarMock(String color) {

        static BrandCar brand;
        static String model;
        
            public CarMock(String color)
            {
                this.color = color;
                //BrandCar        
                this.brand = BrandCar.values()[random.create().nextInt(BrandCar.values().length -1)];
                this.model = "TEST MODEL";
            }

    }


}
