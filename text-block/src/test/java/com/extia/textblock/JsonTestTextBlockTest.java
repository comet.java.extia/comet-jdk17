package com.extia.textblock;

import java.util.Arrays;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;

import com.extia.infra.jpa.Car;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import io.quarkus.test.TestTransaction;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
@TestTransaction
public class JsonTestTextBlockTest {

    @BeforeAll
    public static void setupAll() {
        System.out.println(Banner.JSON.getBanner());
    }

    /**
     * Old way to print JSON object list
     */
    static final String garage_old_fashion = "[{" +
            "\"brand\":\"TESLA\"," +
            "\"model\":\"S\"," +
            "\"color\":\"YELLOW\"" +
            "},{" +
            "\"brand\":\"TESLA\"," +
            "\"model\":\"S\"," +
            "\"color\":\"RED\"" +
            "},{" +
            "\"brand\":\"DODGE\"," +
            "\"model\":\"VIPER\"," +
            "\"color\":\"BLUE\"" +
            "}"
            + "]";

    /**
     * new way more prittier to print JSON object list
     */
    static final String garage_new_fashion = """
            [
                {
                    "brand":"TESLA",
                    "model":"S",
                    "color":"YELLOW"
                },
                {
                    "brand":"TESLA",
                    "model":"S",
                    "color":"RED"
                },
                {
                    "brand":"DODGE",
                    "model":"VIPER",
                    "color":"BLUE"
                }
            ]""";

    /**
     * We launch this test with to way of string building
     * No differences here
     * 
     * @param json
     */
    @ParameterizedTest
    @ValueSource(strings = { garage_old_fashion, garage_new_fashion })
    void inject_json(String json) {
        Car[] readEntities;
        try (Jsonb jsonb = JsonbBuilder.create()) {
            readEntities = jsonb.fromJson(json, Car[].class);
            Arrays.stream(readEntities)
                    .forEach((car) -> System.out.println(car.toString()));
            ;
        } catch (Exception e) {
            e.printStackTrace();
            readEntities = new Car[0];
        }

       
    }

}
