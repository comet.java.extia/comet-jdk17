# jdknew Project

This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:
Maven 3.8+ and Java 17 require.
```shell script
mvn compile quarkus:dev
```

## Blob text
You can find All use cases in src/test/com/extia/textblock

Run ```shell script 
mvn test
```
 or ````shell script
 mvn clean compile quarkus:dev
 ```
 and in prompt input o (for output testing result) and r (for resume test)
