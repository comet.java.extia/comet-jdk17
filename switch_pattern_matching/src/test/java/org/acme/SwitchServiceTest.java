package org.acme;

import org.acme.model.*;
import org.junit.jupiter.api.Test;

public class SwitchServiceTest {

    SwitchService switchService = new SwitchService();

    @Test
    public void withSealedClass() {
        switchService.withSealedClass(new Car(125215));
        switchService.withSealedClass(new Truck(354126));
        switchService.withSealedClass(new Motorcycle(43000));
    }

    @Test
    public void withNull() {
        switchService.withNull(null);
        switchService.withNull("A String !");
        switchService.withNull(new Car(123456));
    }

    @Test
    public void withGuardedPattern() {
        switchService.withCondition(new Car(123456));
        switchService.withCondition(new Car(3450000));
    }
}