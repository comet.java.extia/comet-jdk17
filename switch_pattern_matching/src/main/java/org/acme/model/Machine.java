package org.acme.model;

public abstract sealed class Machine permits Car, Motorcycle, Truck {

    int nbOfWheels;

    int mileage;

    public Machine(int nbOfWheels,  int mileage) {
        this.nbOfWheels = nbOfWheels;
        this.mileage = mileage;
    }

    public int getNbOfWheels() {
        return nbOfWheels;
    }

    public int getMileage() {
        return mileage;
    }
}
