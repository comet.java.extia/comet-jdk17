package org.acme.model;

public final class Truck extends Machine {

    public Truck(int mileage) {
        super(6, mileage);
    }
}
