package org.acme.model;

public non-sealed class Car extends Machine {

    public Car(int mileage) {
        super(4, mileage);
    }

}
