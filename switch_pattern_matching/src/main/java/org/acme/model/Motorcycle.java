package org.acme.model;

public final class Motorcycle extends Machine {

    public Motorcycle(int mileage) {
        super(2, mileage);
    }
}
