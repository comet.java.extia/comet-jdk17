package org.acme;

import org.acme.model.Car;
import org.acme.model.Machine;
import org.acme.model.Motorcycle;
import org.acme.model.Truck;

public class SwitchService {

    public void withSealedClass(Machine machine) {
        switch (machine) {
            case Car c -> System.out.println("This is a car with " + c.getNbOfWheels() + " wheels !");
            case Truck t -> System.out.println("This is a truck with " + t.getNbOfWheels() + " wheels !");
            case Motorcycle c -> System.out.println("This is a car with " + c.getNbOfWheels() + " wheels !");
        }
    }

    public void withNull(Object o) {
        switch (o) {
            case null, String s -> System.out.println("String: " + s);
            default -> System.out.println("Don't know the object");
        }
    }

    public void withCondition(Machine machine) {
        switch (machine) {
            case Car c && c.getMileage() > 200000 -> System.out.println("This is an old car with " + c.getMileage() + "km");
            case Car c -> System.out.println("This car is under 200 000km with " + c.getMileage() + "km");
            case Truck t -> System.out.println("This is a truck with " + t.getNbOfWheels() + " wheels !");
            case Motorcycle c -> System.out.println("This is a car with " + c.getNbOfWheels() + " wheels !");
        }
    }
}
